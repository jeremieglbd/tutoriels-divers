---
author: Jérémie Guilbaud
date: 2 décembre 2019
title: Connexion SSH
---

# Présentation SSH

Le Secure Shell (SSH) est à la fois un programme informatique et un protocole
de communication sécurisé.
Le protocole de connexion impose un échange de clés de chiffrement en début
de connexion.
Par la suite, tous les segments TCP sont authentifiés et chiffrés.
Il devient donc impossible d'utiliser un sniffer pour ce que fait l'utilisateur.
Le protocole SSH a été conçu avec l'objectif de remplacer les différents
protocoles non chiffrés comme rlogin, telnet, rcp et rsh. 
[wikipedia](https://fr.wikipedia.org/wiki/Secure_Shell)

# Connexion

Pour se connecter, la commande `ssh` est utilisée.
Il est ensuite nécessaire de spécifier le serveur sur lequel l'utilisateur
souhaite se connecter :

``` bash
$ ssh serveur
```

Un mot de passe pourra être demandé pour établir la connexion.

Cette commande permettra d'ouvrir une connexion vers le serveur en tant 
qu'utilisateur actuel.
Il est également possible de se connecter avec un autre compte utilisateur.
Cela sera spécifié de la manière suivante :

``` bash
$ ssh nom_utilisateur@serveur
```

# Transfert de fichiers

La commande bash utilisée pour transférer des fichiers en ssh est `scp`.
Elle peut être utilisée de la manière suivante pour copier un fichier depuis
la machine locale vers le serveur distant :

``` bash
$ scp local_file nom_utilateur@serveur:destination/local_file
```

Il est possible de spécifier le port à utiliser en ajoutant l'argument -P.

Pour transferer un fichier depuis le serveur distant vers la machine locale,
la commande suivante est utilisée :

``` bash
$ scp nom_utilisateur@serveur:distant_file destination/distant_file
```
