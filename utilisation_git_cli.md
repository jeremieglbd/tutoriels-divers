---
author: Jérémie Guilbaud
date: 12 novembre 2019
title: Utilisation de Git en lignes de commande
---

# Configuration de Git

On peut utiliser une aide pour avoir des informations sur les commandes :

``` bash
git help config
git help push
git help pull
git help branch
```

On commence par créer son identité, ce qui permettra de déterminer de
qui provient les commit :

``` bash
# nom de l'utilisateur
git config --global user.name "nom_utilisateur"

# adresse mail de l'utilisateur
git config --global user.email "adress_utilisateur"

# ajout de la clé gpg
git config --global user.signingkey id_clé_gpg
```

# Clonage d'un dépôt

Pour récupérer un dépôt en ligne et en faire une copie de travail
local, la commande suivante doit être exécutée :

``` bash
git clone adresse_depot.git
```

L'adresse du dépôt à utiliser doit se terminer par .git, elle est
obtenu sur la page Gitlab ou Github du projet.
On peut récupérer le projet via une adresse HTTPS ou SSH.
Cette dernière option nécessite la génération d'une clé SSH ou l'utilisation
d'une clé déjà existante.

# Vérification du status du dépôt

Pour afficher la liste des fichiers modifiés ainsi que les fichiers qui
doivent encore être ajoutés :

``` bash
git status
```

# Ajout d'un fichier au dépôt

Quand on souhaite ajouter un nouveau fichier au dépôt, il faut l'ajouter à
l'index du projet.
Cela se fait avec la commande :

``` bash
git add nom_fichier
```

# Supression d'un fichier du dépôt

``` bash
git rm nom_fichier
```

# Commit et envoie vers dépôt distant

Une fois des modifications réalisées sur le dépôt local, il faut les commit
pour ensuite les envoyer sur le dépôt distant.

``` bash
git commit -m "message du commit"
```

Le commit en tant que tel n'envoie pas les fichiers, il doit être suivi par une autre commande.
On peut faire plusieurs commit avant d'envoyer les changements vers le
dépôt distant, avec la commande suivante :

``` bash
git push
```

On peut annuler un commit avec la commande suivante :

``` bash
git reset HEAD^
```

On peut revenir à différents commit :

* Dernier commit : HEAD
* Avant-dernier commit : HEAD^
* Avant-avant-dernier commit : HEAD^^

L'annlution d'un commit n'annule pas les modifications réalisées sur
le fichier.
Pour revenir à l'état d'un commit particulier et perdre tous les changements,
on utilise la commande :

``` bash
git reset --hard HEAD^
```

Les changements induits par un commit peuvent également être annulés en utilisant
le hash du commit en question.
La première étape consiste à récupérer les hash des différents commit :

``` bash
$ git log --pretty=oneline
```

Cette commande indique en sortie les différents commit réalisés sur le dépôt,
par ordre chronologique et en indiquant les commentaires associés.
Pour annuler les changements d'un commit en particulier, il suffit de prendre le hash
du commit en question et de lancer :

``` bash
$ git revert <hash>
```

# Mise à jour du dépôt local

Pour récupérer les nouvelles données à partir d'un dépôt local,
sans les intégrer dans les fichiers de travail, on utilise la commande :

``` bash
git fetch origin
```

Cette commande est sans danger et peut être utilisée à n'importe quel
moment pour voir les changements qui ont eu lieu dans le dépôt distant,
sans altérer ses fichiers de travail.

On peut aussi récupérer les nouvelles données et les intégrer dans les
fichiers de travail.
Cette technique peut mener à des erreurs qui seront à résoudre.
Il faut donc veiller à l'utiliser dans des conditions particulières,
après avoir fait un commit de toutes les modifications réalisées.

``` bash
git pull origin master
```

# Remise du dépôt local dans l'état du dépôt distant

Toutes les modifications sur le dépôt local dans des fichiers faisant partie
de l'index seront perdues.

``` bash
git reset --hard origin/master
```
