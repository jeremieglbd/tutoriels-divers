---
author: Jérémie Guilbaud
date: 12 novembre 2019
title: Configuration du compte Gitlab (clé GPG, SSH)
---

# GPG

Lister les clés GPG :

``` bash
gpg --list-keys
```

Exporter la clé publique de la clé GPG :

``` bash
gpg --export -a "guilbaud.jeremie@tutanota.com" > public.key
```

Copier la clé publique et l'ajouter au compte Gitlab.

# SSH

Si une clé SSH n'a pas déjà été générée pour le compte, on en
génère une :

``` bash
ssh-keygen -t ed25519 -C "guilbaud.jeremie@tutanota.com"
```

L'argument -C permet d'ajouter un commentaire, si nécessaire.

On peut cliquer sur « Entrer » pour conserver le chemin par défaut
et ajouter un mot de passe.
Si nécessaire, on peut utiliser la commande suivannte pour ajouter
manuellement un mot de passe à une clé SSH :

``` bash
ssh-keygen -p -f <keyname>
```

Avec le paquet xclip, on peut copier la clé ssh publique dans le
presse-papier :

``` bash
xclip -sel clip < ~/.ssh/id_ed25519.pub
```

Ajouter la clé publique dans Gitlab.

# Utiliser une clé SSH existante

Pour utiliser une clé SSH existante, il faut la copier-coller dans le
répertoire `~/.ssh`, avec les bonnes autorisations :

``` bash
mkdir ~/.ssh
chmod 700 ~/.ssh
cp key ~/.ssh
chmod 600 ~/.ssh/key
chmod 644 ~/.ssh/key.pub
```
