---
title: Création d'une clé USB bootable Linux
author: Jérémie Guilbaud
date: 8 décembre 2019
---

# Préparation de la clé USB

Pour commencer, brancher la clé USB à utiliser.
**Toutes les données présentes dessus seront perdues.**

Lancer la commande `lsblk` pour récupérer la liste des périphériques et partitions :

``` bash
$ lsblk
```

Cette commande donne en sortie la liste des périphériques ainsi que leur partition,
et devrait ressembler à ça :

``` bash
NAME                                          MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT 
sda                                             8:0    1   7.2G  0 disk 
  sda1                                          8:1    1     2G  0 part 
  sda2                                          8:2    1   2.4M  0 part 
nvme0n1                                       259:0    0 119.2G  0 disk 
  nvme0n1p1                                   259:1    0   200M  0 part  /boot/efi 
  nvme0n1p2                                   259:2    0     1G  0 part  /boot 
  nvme0n1p3                                   259:3    0  70.2G  0 part 
    luks-4b11f5bf-7368-47cb-9ea3-049a5b70eaa1 253:1    0  70.2G  0 crypt /home 
  nvme0n1p4                                   259:4    0    40G  0 part 
    luks-6d60654f-7ddf-4010-b8b7-c1c71a73b7f9 253:0    0    40G  0 crypt / 
  nvme0n1p5                                   259:5    0   7.8G  0 part  [SWAP] 
```

Dans cette sortie, la clé USB correspond à `sda`, on peut se baser sur sa taille
pour l'identifier.
Le MOUNTPOINT peut aussi donner des informations sur le nom de la clé USB.

Une fois la clé USB identifiée, il est nécessaire de la démonter pour pouvoir
la formater :

``` bash
sudo umount /dev/sdxX
```

avec `x` la lettre du périphérique (`a` dans notre exemple) et `X` le numéro
de la partition (`1` dans notre exemple).

# Écriture de l'image ISO sur la clé USB

Une fois la clé USB identifiée et démontée, l'image ISO du système d'exploitation
Linux peut être écrite dessus avec la commmande ci-dessous.
On veillera à ne pas donner de numéro de partition et à correctement spécifier
le chemin d'accès à l'ISO.

``` bash
sudo dd bs=4M if=/chemin/vers/image.iso of=/dev/sdx status=progress oflag=sync
```

L'écriture prendra quelques minutes et il sera possible de suivre son avancement
avec la taille copiée et la vitesse d'écriture.

A l'issue de l'écriture, la sortie donnera des informations concernant l'écriture :

``` bash
162+1 records in 
162+1 records out 
681574400 bytes (682 MB, 650 MiB) copied, 142.224 s, 4.8 MB/s 
```
