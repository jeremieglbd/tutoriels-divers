---
author: Jérémie Guilbaud
title: Déployement de Gitlab avec Docker
date: 20 juin 2020
---

[Source](https://docs.gitlab.com/omnibus/docker/)

# Préparation du conteneur

Trois volumes seront utiliées pour le stockage de différents éléments
de l'instance Gitlab conteneurisée :

* config : stockage des fichiers de configuration et les informations
chiffrées concernant l'identification à double facteur, les « variables
sécurisées » CI/CD et autres ;
* log : stockage des fichiers de log de l'instance Gitlab ;
* data : stockage des autres données de l'instance.

# Lancement du conteneur

La commande suivante nécessite d'avoir configuré la variable $GITLAB_HOME,
il est également possible de la remplacer par un chemin d'accès en dur.

``` bash
sudo docker run --detach \
  --hostname gitlab.example.com \
  --publish 443:443 --publish 80:80 --publish 22:22 \
  --name gitlab \
  --restart always \
  --volume $GITLAB_HOME/config:/etc/gitlab:Z \
  --volume $GITLAB_HOME/logs:/var/log/gitlab:Z \
  --volume $GITLAB_HOME/data:/var/opt/gitlab:Z \
  gitlab/gitlab-ce:latest
```

* detach : lance le conteneur en mode daemon en arrière-plan ;
* hostname : l'url permettant d'accèder à l'instance.
Il est également possible d'indiquer une adresse IP pour un usage sans
https.
* publish 443:443 : redirige le port 443 de la machine hôte vers le conteneur
pour une connexion en https ;
* publish 80:80 : redirige le port 80 de la machine hôte vers le conteneur pour
une conenxion en http ;
* publish 20:20 : redirige le port 20 de la machine hôte vers le conteneur pour
une connexion ssh ;
* name : le nom du conteneur, ici `gitlab`.
Il est possible de changer le nom, mais il faudra adapter les commandes
dans le reste de ce tutoriel avec le nouveau nom.
* restart always : force le redémarrage du conteneur dans les cas de figure ;
* volume : monte les différents volumes utilisées par le conteneur ;
* gitlab/gitlab-ce:latest : l'image Docker utilisée pour l'instance Gitlab.

Les ports de la machine hôte peuvent être modifiées de la sorte : par exemple,
pour accèder au conteneur en ssh depuis le port 2222 de la machine hôte, 
le paramètre deviendrait : ```--pusblish 2222:22```.

Pour suivre le déploiement du conteneur, la commande suivante est disponible :

`sudo docker logs -f gitlab`

# Configuration de l'instance

La configuration pour l'instance Gitlab est placé dans le répertoire
`/etc/gitlab/gitlab.rb` dans le conteneur.
Pour l'éditer, la commande suivante peut être utilisée :

`sudo docker exec -it gitlab /bin/bash`

Une fois la configuration effectuée, le conteneur doit être relancé avec la
commande suivante :

`sudo docker restart gitlab`

# Sauvegarde

L'instance Gitlab peut être sauvegardée facilement avec la commande suivante :

```
docker exec -t gitlab gitlab-backup create
```

L'argument `STRATEGY=copy` peut être ajouté à la commande ci-dessus.
Cela permet de faire une copie des répertoires à sauvegarder dans un
répertoire temporaire et créer l'archive après.
Cela nécessite cependant de faire une copie de toute l'instance gitlab,
ce qui peut causer des ralentissements.

Pour faire des sauvegardes incrémentielles plus rapidement avec rsync, 
il est également possible
de changer le nom de l'archive avec l'argument `BACKUP=nom_archive`.
Cela donnera une archive appelée `nom_archive_gitlab_backup.tar`.
L'argument `GZIP_RSYNCABLE=yes` peut être ajouté pour faciliter
l'utilisation de rsync par la suite.

Avec tous ces arguments, la commande de sauvegarde devient :

```
docker exec -t gitlab gitlab-backup create \
	STRATEGY=copy \
	BACKUP=dump \
	GZIP_RSYNCABLE=YES
```

La sauvegarde se trouvera sous la forme d'un fichier .tar dans le répertoire 
`$GITLAB_HOME/data/backups/`.
A noter que les fichiers de configuration ne sont pas sauvegardées de cette
manière, il faudra directement faire une sauvegarde des fichiers
gitlab-secrets.json et gitlab.rb présents dans le dossier 
`$GITLAB_HOME/config/`.

# Restauration

Les répertoires de restauration doivent être vides pour ne pas avoir de
problèmes de droit d'accès.
Le fichier de sauvegarde .tar doit être accessible dans le répertoire
`/var/opt/gitlab/backups/` dans le conteneur.

Pour restaurer la sauvegarde, la commande suivante doit être exécutée :

`docker exec -it gitlab gitlab-backup restore`

Les fichiers de configuration peuvent ensuite être restaurés en les
plaçant dans le répertoire correspodant, et l'instance Gitlab
peut être redémarrée :

```
sudo docker exec -t gitlab gitlab-ctl reconfigure
sudo docker exec -t gitlab gitlab-ctl restart
sudo docker exec -t gitlab gitlab-rake gitlab:check SANITIZE=true
```
