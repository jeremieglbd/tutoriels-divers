# Présentation

Ce dépôt contient des tutoriels rédigés pour différentes thématiques liées
à l'univers Linux.

Les tutoriels sont rédigés en français, et disponibles sous la forme de
fichiers pdf et md.
La police utilisée dans les fichiers pdf est 
[Libertinus](https://en.wikipedia.org/wiki/Linux_Libertine#Derivative_works),
une police distribuée avec la licence GPL/OFL.
