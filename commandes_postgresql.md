---
title: Commandes PostgreSQL
author: Jérémie Guilbaud
date: 22 janvier 2020
---

# Lister les bases de données

``` bash
psql -l
```

Le nom des bases, leur propriétaire, l'encodage et d'autres informations sont
affichées.

# Accèder à une base de données :

``` bash
psql -d nom_de_la_base
```

L'invite de commande passe à ```nom_de_la_base=#``` et permet l'exécution
de commandes sans rajouter le préfix `psql`.
Les commandes du type ```psql -l``` sont simplement exécutées avec ```\l```.
La version de postgresql est affichée.
Pour quitter l'invite de commande psql, exécuter ```\q```.

# Changement de base de données

```
\c nom_base
```

# Lister les tables

``` bash
\dt
```
