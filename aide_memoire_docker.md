---
title: Aide mémoire commandes Docker
author: Jérémie Guilbaud
date: 4 février 2020
---

# Images Docker

## Récupération d'une image depuis [Dockerhub](https://hub.docker.com)

``` bash
docker pull <nom image>[:tag]
```

## Liste des images

``` bash
docker images
```

## Création d'une image depuis un Dockerfile

Nécessite de se placer dans le dossier contenant l'image.

``` bash
docker build -t <nom image>[:tag] .
```

## Suppression d'une image

``` bash
docker rmi <ID ou nom image>[:tag]
```

# Conteneurs Docker

## Création d'un conteneur

Le conteneur peut être créé à partir d'une image existante ou d'une image
qui sera récupérée sur le Dockerhub à l'exécution de la commande.

``` bash
docker run [options] <ID ou nom image>[:tag] [commandes] [arguments]
```

Différentes options peuvent être ajoutées à cette commande, elles doivent
être placées avant le nom ou ID de l'image à lancer.

* `--name` : donne un nom au conteneur créé ;
* `-d` : lance le conteneur en arrière plan (daemon) ;
* `--rm` : supprime le conteneur à son arrêt ;
* `-p` : spécifie les ports utilisés par Docker avec le <port hôte>:<port conteneur> ;
* `-e` : spécifie des variables d'environnement ;
* `-i` : lancement du conteneur en mode intéractif ;
* `-t` : alloue un pseudo-tty et l'attache à l'entrée standard.

Pour accèder au conteneur à son lancement avec une invite de commande, rajouter
`/bin/bash` après le nom ou ID de l'image dans la commande de lancement et 
`-t -i` dans les options.

## Lister les conteneurs

Conteneurs en cours de fonctionnement :

``` bash
docker ps
```

Tous les conteneurs, incluant ceux arrêtés :

``` bash
docker ps -a
```

## Accèder à un conteneur en cours de fonctionnement

``` bash
docker exec -it <ID ou nom conteneur> /bin/bash
```

## Lister les propriétés d'un conteneur

``` bash
docker inspect <ID ou nom conteneur>
```

## Stopper un conteneur

Si le conteneur a été lancé avec l'argument `--rm`, il sera également
supprimé.

``` bash
docker stop <ID ou nom conteneur>
```

## Lancer un conteneur stoppé

``` bash
docker start <ID ou nom conteneur>
```

## Supprimer un conteneur

Nécessite préalablement qu'il soit arrêté.

``` bash
docker rm <ID ou nom conteneur>
```

# Volumes

Pour lancer un conteneur en partageant un fichier avec la VM :

``` bash
docker run -v /chemin/dossier/hôte:/chemin/dossier/conteneur <id nom image>
```

Des problèmes d'accès à la machine peuvent survenir avec SELinux.
Pour les résoudres :

``` bash
chcon -Rt svirt_sandbox_file_t <nom dossier partagé hôte>
```
