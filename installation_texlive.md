---
author: Jérémie Guilbaud
title: Installation de TeXlive
date: 12 novembre 2019
---

On récupère l'[installateur](https://www.tug.org/texlive/acquire-netinstall.html)
en ligne (archive install-tl-unx.tar.gz).

On dezippe l'archive :

``` bash
tar -xzvf install-tl-unx.tar.gz
```

On se rend dans le repertoire créé et on donne
les droits d'exécution pour l'installateur :

``` bash
chmod +x install-tl
```

Lors de l'installation, des problèmes de droits d'accès peuvent apparaitre.
Pour les éviter on créé le répertoire de texlive en amont de l'installation,
avec les droits adaptés :

``` bash
sudo mkdir /usr/local/texlive && sudo chown 'whoami' texlive
```

Cela évitera d'avoir à installer texlive en tant que sudo.
On lance l'installateur avec :

``` bash
./install-tl
```

On laisse tout par défaut en vérifiant que texlive s'installe dans
usr/local et on valide en entrant « I ».

A la fin de l'installation, on ajoute le PATH de texlive dans le
fichier .bashrc :

``` bash
export PATH=/usr/local/texlive/2019/bin/x86_64-linux:$PATH
```
