---
title: Configuration d'un VPN avec openvpn
author: Jérémie Guilbaud
date: 13 décembre 2019
---

# Installation

Installer openvpn avec la commande suivante :

``` bash
sudo dnf install openvpn
```

# Configuration

Placer le fichier de configuration en .ovpn dans le répertoire 
`etc/openvpn`.
Lancer la connexion au VPN avec :

``` bash
sudo openvpn --config /etc/openvpn/*.ovpn
```

Pour changer le serveur DNS, éditer le fichier `/etc/resolv.conf`.
Ajouter ou enlever des serveurs DNS en ajoutant ou supprimant des
nameserver.
Les changements seront cependant supprimés par NetworkManager, une solution
pour éviter cela est d'éditer le fichier `/etc/NetworkManager/NetworkManager.conf`
et d'ajouter `dns=none` dans la section `[main]`.
