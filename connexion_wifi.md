---
author: Jérémie Guilbaud
date: 19 novembre 2019
title: Connexion wifi en lignes de commande
---

Nécessite d'avoir l'outil « NetworkManager » installé.

# État de la connexion wifi

``` bash
nmcli general status
```

# Activer ou désactiver la connexion wifi

``` bash
nmcli networking on
```

``` bash
nmcli networking off
```

# Lister les réseaux wifi connus


``` bash
nmcli c
```

Pour se connecter à un de ces réseaux, utiliser la commande :

``` bash
nmcli c up id <id connexion>
```

# Nouvelle connexion

``` bash
nmcli dev wifi connect <SSID> password <Password>
```

# Supprimer une connexion

``` bash
nmcli c del <id connexion>
```

