---
author: Jérémie Guilbaud
date: 7 juillet 2020
title: Retouche d'images avec ImageMagick
---

# Remplacement d'une couleur par une autre

```bash
convert -fuzz XX% -fill red -opaque white image.png resultat.png
```

Dans cet exemple, la couleur rouge est remplacée par du blanc.
Il est possible de remplacer une nuance de couleur en augmentant
le pourcentage `-fuzz`.
Pour ne remplacer qu'une couleur précise, l'argument `-fuzz` peut être
enlevé ou défini à 0 %.

Pour mettre un fond blanc sur une image scannée avec du texte, la commande
suivante peut être utilisée :

```bash
convert -fuzz 20% -fill "#ffffff" -opaque "#f2f2f2" image.png resultat.png
```
