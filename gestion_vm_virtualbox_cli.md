---
title: Gestion de machines virtuelles avec VirtualBox en lignes de commande
date: 6 janvier 2020
author: Jérémie Guilbaud
---

# Lister machines virtuelles (VM)

Pour lister toutes les VMs disponibles :

``` bash
VBoxManage list vms
```

Pour lister toutes les VMs démarrées :

``` bash
VBoxManage list runningvms
```

# Contrôle de l'état d'une VM

Démarrage d'une VM avec un écran attaché :

``` bash
VBoxManage startvm "nom_vm"
```

Cette commande ne fonctionne pas si elle est lancée en SSH.
Dans ce cas, il faut lancer la VM en mode _headless_, sans affichage :

``` bash
VBoxManage startvm "nom_vm" --type headless
```

Arrêt d'une VM :

``` bash
VBoxManage controlvm "nom_vm" poweroff
```

# Propriétés d'une VM

Pour lister les propriétés d'une VM :

``` bash
VBoxManage showinfo "nom_vm"
```

Le nom de la VM doit être entre guillemets et est récupéré en listant
les VMs.
L'ID de la VM, obtenu de la même façon, peut également être utilisé pour
identifier la VM, sans avoir besoin d'utiliser les guillemets.

Modification d'une propriété d'une VM :

``` bash
VBoxManage modifyvm "nom_vm" --<propriété> <valeur>
```

Par exemple, pour modifier la mémoire vive allouée à une VM, la commande
suivante est utilisée :

``` bash
VBoxManage modifyvm "nom_vm" --memory 12000
```

La VM nom_vm se verra allouer 12 000 Mo de mémoire vive dans cet exemple.

